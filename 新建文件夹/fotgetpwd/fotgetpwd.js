const app = getApp()
Page({
  data: {
    password: '',
    mobile: '',
    // 验证码 还没验证 必填的时候 不验证它了 后期加上
    nowTel: '',
  },
  validatemobile: function (mobile) {
    this.mobile = mobile.detail.value;
    console.log(mobile, '33')
    if (mobile.detail.value === '') {
      wx.showToast({
        title: '请输入手机号！',
        icon: 'success',
        duration: 1500
      })
      return false;
    }
    if (mobile.detail.value.length != 11) {
      wx.showToast({
        title: '手机号长度有误！',
        icon: 'success',
        duration: 1500
      })
      return false;
    }
    var myreg = /^1[34578][0-9]{9}$/;
    if (!myreg.test(mobile.detail.value)) {
      wx.showToast({
        title: '手机号有误！',
        icon: 'success',
        duration: 1500
      })
      return false;
    }
    return true;
  },
  password: function (password) {
    console.log(password, '331111111111111111111')
    this.password = password.detail.value
    console.log(this.password, 'sssss')
    if (password.detail.value === '') {
      wx.showToast({
        title: '请输入密码！',
        icon: 'success',
        duration: 1500
      })
      return false;
    }
    if (password.detail.value.length <= 6 || password.detail.value.length >= 10) {
      wx.showToast({
        title: '请输入6-10位密码！',
        icon: 'success',
        duration: 1500
      })
      return false;
    }
    var myreg = /^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,10}$/;
    if (!myreg.test(password.detail.value)) {
      wx.showToast({
        title: '请输入6-10位密码字母加数字！',
        icon: 'success',
        duration: 1500
      })
      return password.detail.value
      return false;
    }
    return true;
  },
  passwordNow: function (nowTel) {
    this.nowTel = nowTel.detail.value;
    console.log(nowTel, '3333444444444444')
    console.log(this.password, '1111111111111111')
    if (nowTel.detail.value != this.password) {
      wx.showToast({
        title: '请输入同上的密码！',
        icon: 'success',
        duration: 1500
      })
    }
  },
  formBindsubmit: function () {
    if (this.password != '' || this.mobile != '' || this.nowTel != '') {
      wx.showToast({
        title: '请输入完整！',
        icon: 'success',
        duration: 1500
      })
    }
  }
})