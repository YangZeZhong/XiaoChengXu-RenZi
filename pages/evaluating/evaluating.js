const app = getApp()
Page({
  data: {
    is_eq: false,
    is_iq: false,
    is_aq: false
  },
  openEq: function () {
    wx.navigateTo({
      url: '/pages/evaluating/evaluating_eq',
    })
  },
  openIq: function () {
    wx.navigateTo({
      url: '/pages/evaluating/evaluate_iq',
    })
  },
  openAq: function () {
    wx.navigateTo({
      url: '/pages/evaluating/evaluate_aq',
    })
  }
})
export default {
  evalu: function (name, api) {
    var appInstance = getApp();
    Page({
      data: {
        score: ''
      },
      onLoad: function () {
        var that = this;
        wx.request({
          url: api,
          success: function (e) {
            that.setData({
              data: e.data.data
            })
            that.setData({
              number: e.data.data.length
            })
          }
        })
      },
      // 重置表单
      formReset: function () {},
      // 分数
      formSubmit: function (e) {
        // 获取所有需要验证的字段
        var valiKey = e.detail.value;
        var valiRule = {}
        var valiMes = {}
        for (var k in valiKey) {
          // 获取所有必填字段
          valiRule[k] = { required: true }
          valiMes[k] = { required: '请完成所有选项' }
        }
        this.WxValidate = appInstance.wxValidate(
          valiRule,
          valiMes
        )
        //提交错误描述
        if (!this.WxValidate.checkForm(e)) {
          const error = this.WxValidate.errorList[0]
          // `${error.param} : ${error.msg} `
          wx.showToast({
            title: `${error.msg} `,
            icon: 'none',
            duration: 800
          })
          return false
        }
        var that = this;
        var score = 0;
        for (var k in e.detail.value) {
          if (e.detail.value[k] == '') {
            e.detail.value[k] = 0
          }
          score = score + parseInt(e.detail.value[k])
        }
        that.setData({
          score: score
        })
        wx.navigateTo({
          url: '/pages/evaluating/evaluate_result?name=' + name + '&score=' + that.data.score,
        })
      }
    })
  }
}