import Api from '../api/api.js'
import base64 from '../../utils/base64.js'
import validate from '../../utils/vallidate.js'
const wxValidate = new validate() 
const Base64 = new base64.Base64() // Base64.encode(a); Base64.decode(b);
const api = Api.api
let data = {
  data: {
    trueCode: "",
    sendCode:"发送验证码",
    countFlag:false,
    checked: 'password',
    countCode:null,
    required: ["phone","code",'password'],
    passIcon: false,
    focus: false,
  },
  showToast: function (text,fn) {
    wx.showToast({
      title: text,
      icon: 'none',
      duration: 1000,
      success:fn
    })
    return false;
  },
  // 验证form
  validate: function (e) {
    wxValidate.validate(e,this)
    this.setData({ [e.currentTarget.dataset.name]:e.detail.value})
  },
  // 用户名密码 提交form
  submitByPass: function (e) {
    wxValidate.checkRequired(e, this, this.data.required);
    let subType = e.detail.target.dataset.submit;
    let data = this.data;
    let value = e.detail.value;
    let that = this;
    if (data.form.$invalidMsg != true) { return false; }
    else if (data.trueCode === "") { this.showToast('请发送验证码'); return false; }
    else {
      wx.request({
        url: api['MOBILE_ACCOUNT_REG__POST'],
        method: "POST",
        data: {
          'mobile': value.phone,
          'code': value.code,
          'password': Base64.encode(value.password),
          'inviterId': "37"
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success: function (res) {
          //that.showToast(res.data.msg ? res.data.msg :'404，服务器错误')
          wx.showLoading({title: '注册成功',})
          setTimeout(function () {
            wx.hideLoading()
            wx.navigateTo({ url: '/pages/login/login' })
          }, 1000)
        },
        fail: function (res) {
          console.log(res)
        }
      })
    } 
  },
  // 发送验证码
  send_code: function (e,event) {
    let that = event ? event : this;
    let data = that.data;
    let type = e.currentTarget.dataset.type;
    let count = 61;
    let self = this;
    let promise = new Promise(function (resolve, reject) {
      if (data.form && data.form.phone === true || data.form && data.form.codePhone === true) {
        wx.request({
          url: api['MOBILE_ACCOUNT_SEND_SMSCODE__POST'],
          method: "POST",
          data: {
            'mobile': Number(type == 1 ? data.phone : data.codePhone),
            'type': type
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded' //application/json
          },
          success: function (res) {
            if (res.data.code != 200) {
              self.showToast(res.data.msg ? res.data.msg:'404，服务器错误')
            }
            else {
              data.trueCode = res.data.data;
              resolve(data.trueCode)
              self.showToast('验证码已发送，请注意查收');
              data.countCode = setInterval(function () {
                count--;
                that.setData({ sendCode: `${count} 秒后重试` });
                that.setData({ countFlag: true })
                if (count === 0) {
                  that.setData({ sendCode: "重新发送" });
                  that.setData({ countFlag: false })
                  clearInterval(data.countCode);
                }
              }, 1000)
              if (!event) {
                wxValidate.addRule([{ code: new RegExp(res.data.data), msg: "验证码有误！" }])
              }
            }
          },
          fail: function (res) {
            self.showToast('网络未连接');
          }
        })
      }
      else {
        self.showToast('请输入正确的手机号！')
        return false
      }
    });
    return promise   
  },
  checkPwd: function (e) {
    this.setData({ passIcon: !this.data.passIcon,checked: this.data.passIcon ? 'text' : 'password',focus:true})
  }
}
Page(data)
export default { data }