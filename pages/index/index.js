const app = getApp()
Page({
  data: {

  },
  openResume: function () {
    wx.navigateTo({
      url: '/pages/resume/resume',
    })
  },
  openRvaulat: function () {
    wx.navigateTo({
      url: '/pages/evaluating/evaluating',
    })
  },
  loginOut: function() {
    wx.removeStorageSync('mobile')
    wx.removeStorageSync('password');
    wx.navigateTo({ url: '/pages/login/login' }) 
  }
})
