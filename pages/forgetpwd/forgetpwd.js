import Api from '../api/api.js'
import base64 from '../../utils/base64.js'
import validate from '../../utils/vallidate.js'
const wxValidate = new validate()
const Base64 = new base64.Base64() // Base64.encode(a); Base64.decode(b);
const api = Api.api
const app = getApp()
let data = {
  data: {
    trueCode: "",
    sendCode: "发送验证码",
    countFlag: false,
    countCode: null,
    required: ["phone", "code", 'password','same'],
    rule: [],
    errorCode:'',
    passIcon:false,
    confirmIcon:false,
    passChecked: 'password',
    confirmChecked:'password',
    focus:false,
    confirmfocus:false
  },
  showToast: function (text) {
    wx.showToast({
      title: text,
      icon: 'none',
      duration: 1000
    })
    return false;
  },
  // 验证form
  validate: function (e) {
    wxValidate.validate(e, this)
    this.setData({ [e.currentTarget.dataset.name]: e.detail.value })
  },
  // 提交form 注册/登录 共用 
  submitByPass: function (e) {
    wxValidate.checkRequired(e, this, this.data.required)
    let data = this.data;
    let value = e.detail.value;
    if (data.form.$invalidMsg != true) { return false; }
    else if (data.errorCode != '') { this.showToast(data.errorCode); return false; }
    else {
      let that = this;
      wx.request({
        url: api['MOBILE_ACCOUNT_PASSWORD_FORGOT__POST'],
        method: "POST",
        data: {
          'mobile': value.phone,
          'code': value.code,
          'new_password': Base64.encode(value.password)
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success: function (res) {
          if(res.data.code!=200){
            that.showToast(res.data.msg)
          }
          else{
            wx.navigateTo({ url: '/pages/login/login' })
          }
        },
        fail: function (res) {
          console.log(res)
        }
      })
    }
  },
  send_code: function (e) {
    let that = this;
    let data = that.data;
    let type = e.currentTarget.dataset.type;
    let count = 61;
    clearInterval(data.countCode);
    if (data.form && data.form.phone === true) {
      wx.request({
        url: api['MOBILE_ACCOUNT_SEND_SMSCODE__POST'],
        method: "POST",
        data: {
          'mobile': Number(data.phone),
          'type': type
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded' //application/json
        },
        success: function (res) {
          if (res.data.code != 200) {
            data.errorCode = res.data.msg
            that.showToast(res.data.msg)
          }
          else {
            data.trueCode = res.data.data;
            that.showToast('验证码已发送，请注意查收');
            data.countCode = setInterval(function () {
              count--;
              that.setData({ sendCode: `${count} 秒后重试` });
              that.setData({ countFlag: true })
              if (count === 0) {
                that.setData({ sendCode: "重新发送" });
                that.setData({ countFlag: false })
                clearInterval(data.countCode);
              }
            }, 1000)
            that.data.rule.push({ code: new RegExp(res.data.data), msg: "验证码有误！" })
            wxValidate.addRule(data.rule)
          }
        },
        fail: function (res) {
          that.showToast('网络未连接');
        }
      })
    }
    else {
      that.showToast('请输入正确的手机号！')
      return false
    }
  },
  checkPwd: function (e) {
    let focus = e.currentTarget.dataset.confirm ? 'confirmfocus' : 'focus'
    let falg = this.data[e.currentTarget.dataset.icon] === false ? true : false;
    this.setData({ [e.currentTarget.dataset.icon]: falg, [e.currentTarget.dataset.name]: falg === false ? 'password' : 'text', [focus]:true})
  }
}
Page(data)
export default { data }