var appInstance = getApp()
Page({
  data: {
    // 所有选项的可选值
    is_disease: [
      { name: '是', value: '0' },
      { name: '否', value: '1' },
    ],
    // 填写简历页面1,2,3,4的表单数据对象
    dataForm: {}
  },
  onLoad: function (e) {
    this.data.dataForm = JSON.parse(e.data)
    console.log(this.data.dataForm, 'form3传递过来的数据');
  },
  formSubmit: function (e) {
    var that=this;
    // 获取所有需要验证的字段
    var valiKey = e.detail.value;
    var valiRule = {}
    var valiMes = {}
    for (var k in valiKey) {
      // 获取所有必填字段
      valiRule[k] = { required: true }
      valiMes[k] = { required: '请完成所有选项' }
    }
    this.WxValidate = appInstance.wxValidate(
      valiRule,
      valiMes
    )
    //提交错误描述
    if (!this.WxValidate.checkForm(e)) {
      const error = this.WxValidate.errorList[0]
      // `${error.param} : ${error.msg} `
      wx.showToast({
        title: `${error.msg} `,
        icon: 'none',
        duration: 800
      })
      return false
    }

    that.data.dataForm.form4 = that.data.dataForm4;
    that.openResume(that.data.dataForm);
  },
  openResume: function (data) {
    wx.navigateTo({
      url: '/pages/resume/form5?data=' + JSON.stringify(data)
    })
  },
  // 下一页
  openForm: function () {
    wx.navigateTo({
      url: '/pages/resume/form5',
    })
  },
  backPage: function () {
    wx.navigateBack({
    })
  }
})
