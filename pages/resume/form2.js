var appInstance = getApp();
var nowDate = new Date();
var month = nowDate.getMonth() + 1;
var day = nowDate.getDay()
Page({
  data: {
    // 性别可选项
    sex: [
      { name: '0', value: '男' },
      { name: '1', value: '女', checked: 'true' },
    ],
    // 出生日期
    bri_date: '',
    // 年龄
    bri_age: '',
    // 用来存储简历页面1,2,3,4,5各表单数据
    dataForm: {}
  },
  // 保存从上一页中传递过来的数据
  onLoad: function (e) {    
    this.data.dataForm = JSON.parse(e.data);
    console.log(this.data.dataForm, 'form1传递过来的数据');
  },
  // 提交表单
  formSubmit: function (e) {
    var that = this;
    // 获取表单验证对象
    this.WxValidate = appInstance.wxValidate(
      {
        sg: {
          required: true,
        },
        tz: {
          required: true,
        },
        mz: {
          required: true,
        },
        csd: {
          required: true
        },
        hj: {
          required: true
        },
        xzz: {
          required: true,
        },
        wt_thy: {
          required: true,
        },
        wt_qypy: {
          required: true,
        }
      },
      {
        sg: {
          required: '请输入身高',
        },
        tz: {
          required: '请输入体重',
        },
        mz: {
          required: '请输入民族',
        },
        csd: {
          required: '请输入出生地',
        },
        hj: {
          required: '请输入紧急联系人姓名',
        },
        xzz: {
          required: '请输入现住址/小区',
        },
        wt_thy: {
          required: '请完善信息',
        },
        wt_qypy: {
          required: '请完善信息',
        }
      })
    // 执行表单验证
    if (!this.WxValidate.checkForm(e)) {
      const error = this.WxValidate.errorList[0]
      // `${error.param} : ${error.msg} `
      wx.showToast({
        title: `${error.msg} `,
        icon: 'none',
        duration: 800
      })
      return false
    }
    // 获取表单提交结果
    if (e.detail.value.xb == null) {
      e.detail.value.xb = 0
    }
    // 组合简历表单对象，传递到下一个页面
    that.data.dataForm.form2 = e.detail.value;
    that.openResume(that.data.dataForm)
  },
  openResume: function (data) {
    wx.navigateTo({
      url: '/pages/resume/form3?data=' + JSON.stringify(data),
    })
  },
  // 失去焦点时验证一次正确则获取出生日期获取出生日期
  get_bri: function (e) {
    var idcard = e.detail.value;
    console.log(nowDate, month)
    if (/^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|[xX])$/.test(idcard)) {
      var bri_year = idcard.substring(6, 10);
      var bri_month = idcard.substring(10, 12);
      var bri_day = idcard.substring(12, 14)
      this.setData({
        bri_date: bri_year + "-" + bri_month + "-" + bri_day,
        bri_age: nowDate.getFullYear() - bri_year
      })
    } else {
      wx.showToast({
        title: '请输入正确身份证号',
        icon: 'none',
        duration: 800
      })
    }
    //提交错误描述
  },
  // 返回
  backPage: function () {
    wx.navigateBack({
    })
  }
})
