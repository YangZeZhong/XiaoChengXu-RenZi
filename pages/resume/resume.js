const app = getApp()
Page({
  data: {

  },
  // 我同意按钮，跳转到下一页
  openForm: function () {
    wx.navigateTo({
      url: '/pages/resume/form1',
    })
  },
  // 不同意按钮，返回上一页
  backPage: function () {
    wx.navigateBack({
    })
  }
})