const app = getApp()
Page({
  data: {
    // 照片地址
    photoUrl: '',
    // 简历表单 form1,2,3,4的数据
    dataForm: {}
  },
  onLoad: function (e) {
    this.data.dataForm = JSON.parse(e.data)
    console.log(this.data.dataForm, 'form4传递过来的数据');
  },
  submitForm: function (e) {
    // 拼接个表单字段到一个大对象中
    var that = this;
    var subModel = Object.assign(that.data.dataForm.form1, that.data.dataForm.form2, that.data.dataForm.form3, that.data.dataForm.form4);

    console.log(subModel);
    wx.request({
      url: 'http://192.168.0.180/wechat/resume',
      method: 'POST',
      data: {
        imgUrl: that.data.photoUrl,
        data1: subModel
      },
      success: function (e) {
        console.log(e, '数据返回值');
      }
    })
  },
  // 上传图片
  photoUplod: function () {
    var that = this
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
      success: function (res) {
        // 返回选定照片的本地文件路径列表，tempFilePath可以作为img标签的src属性显示图片
        var tempFilePaths = res.tempFilePaths;
        console.log(res.tempFilePaths);
        that.setData({
          photoUrl: res.tempFilePaths
        })
      }
    })
  },
  // 重置上传图片
  resetPhoto: function () {
    this.setData({
      photoUrl: ''
    })
  },
  backPage: function () {
    wx.navigateBack({
    })
  }
})
