var appInstance = getApp()
Page({
  data: {
    // 应聘岗位可选项
    work: [
      { name: 'zygw', value: '职业顾问', checked: 'true' },
      { name: 'rzzy', value: '人资专员' },
      { name: 'mg', value: '美工' },
      { name: 'qd', value: '前端' },
      { name: 'hd', value: '后端' }
    ],
    // 当前状态可选项
    statue: ['在职', '待业', '应届毕业生', '未毕业'],
    // 信息获取渠道可选项
    information: ['唐山人才网', '58同城', '赶集', '智联', '前程', '英才网', '人才市场', '户外广告', '校园招聘', '熟人介绍', '其他'],
    // 应聘岗位的已选索引
    index: 0,
    // 当前状态的已选择项索引
    statueIndex: 0,
    // 信息获取渠道已选项索引
    informationIndex: 0,
    // 到岗日期
    date: ''
  },
  onLoad: function () {    
  },
  // 提交表单
  formSubmit: function (e) {
    var that = this;
    // 数据验证
    this.WxValidate = appInstance.wxValidate(
      {
        xm: {
          required: true
        },
        sjdh: {
          required: true,
          tel: true,
        },
        qtdh: {
          required: true,
          tel: true,
        },
        qq: {
          required: true
        },
        weixin: {
          required: true
        },
        jjjtlxr_dh: {
          required: true,
          tel: true,
        },
        jjjtlxr_xm: {
          required: true,
        },
        jjjtlxr_gx: {
          required: true,
        },
        jjjtlxr_dz: {
          required: true,
        },
        qwxj: {
          required: true,
        },
        kdzrq: {
          required: true
        },
        xwgzdd: {
          required: true
        }
      },
      {
        xm: {
          required: '请输入您的姓名',
        },
        sjdh: {
          required: '请输入您的手机号',
        },
        qtdh: {
          required: '请输入其他电话',
        },
        qq: {
          required: '请输入QQ',
        },
        weixin: {
          required: '请输入微信号码',
        },
        jjjtlxr_dh: {
          required: '请输入紧急联系人电话',
        },
        jjjtlxr_xm: {
          required: '请输入紧急联系人姓名',
        },
        jjjtlxr_gx: {
          required: '请输入紧急联系人关系',
        },
        jjjtlxr_dz: {
          required: '请输入紧急联系人地址',
        },
        qwxj: {
          required: '请输入期望薪金',
        },
        xwgzdd: {
          required: '请输入希望工作地点',
        },
        kdzrq: {
          required: '请输入可到职日期',
        }
      })
    // 提交错误描述
    if (!this.WxValidate.checkForm(e)) {
      const error = this.WxValidate.errorList[0]
      // `${error.param} : ${error.msg} `
      wx.showToast({
        title: `${error.msg} `,
        icon: 'none',
        duration: 2000
      })
      return false
    }
    // 对空值添加默认值
    if (e.detail.value.dqzt == null || e.detail.value.rhhqypxx == null) {
      e.detail.value.dqzt = 0
      e.detail.value.rhhqypxx = 0
    }
    // 所有表单1的数据
    that.dataForm = { form1: e.detail.value};
    that.openResume(that.dataForm)
  },
  // 携带数据打开下一页
  openResume: function (data) {
    wx.navigateTo({
      url: '/pages/resume/form2?data=' + JSON.stringify(data),
    })
  },
  // 选择应聘岗位已选索引
  candidateChange: function (e) {
    this.setData({
      index: e.detail.value
    })
  },
  // 当前状态已选索引
  statueChange: function (e) {
    this.setData({
      statueIndex: e.detail.value
    })
  },
  // 信息获取渠道已选索引
  informationChange: function (e) {
    this.setData({
      informationIndex: e.detail.value
    })
  },
  // 到岗时间
  bindDateChange: function (e) {
    this.setData({
      date: e.detail.value
    })
  },
  // 返回
  backPage: function () {
    wx.navigateBack({
    })
  }
})
