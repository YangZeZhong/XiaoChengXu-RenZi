var appInstance = getApp()
var nowDate = new Date()
Page({
  data: {
    // 有无工作经验可选项
    work: [
      { name: 'isTrue', value: '有' },
      { name: 'isFalse', value: '无' },
    ],
    // 教育经历时间，工作经历时间 最大限制
    now_date: nowDate,
    // 教育起始时间
    star_date: '',
    // 教育结束时间
    end_date: '',

    isShow: false,    // 展示工作经历
    workExps: [{}],   // 工作经历列表
    dataForm: {},     // 前边form传过来的数据
    dataForm3: {}     
  },
  onLoad: function (e) {
    this.data.dataForm = JSON.parse(e.data)
    console.log(this.data.dataForm, 'form2传递过来的数据');   
  },
  formSubmit: function (e) {
    console.log(e)
    var that = this

    // 获取所有需要验证的字段
    var valiKey = e.detail.value;
    var valiRule = {}
    var valiMes = {}
    for (var k in valiKey) {
      // 获取所有必填字段
      valiRule[k] = { required: true }
      valiMes[k] = { required: '请完成所有选项' }
    }
    this.WxValidate = appInstance.wxValidate(
      valiRule,
      valiMes
    )
    // 表单数据验证
    if (!this.WxValidate.checkForm(e)) {
      const error = this.WxValidate.errorList[0]
      // `${error.param} : ${error.msg} `
      wx.showToast({
        title: `${error.msg} `,
        icon: 'none',
        duration: 800
      })
      return false
    }
    that.data.dataForm3 = JSON.parse(JSON.stringify(e.detail.value));
    // 删除大form对象中的工作经历相关字段
    if (that.data.isShow) {
      for (var k = 0; k < that.data.workExps.length; k++) {
        delete that.data.dataForm3['dwmc' + k]
        delete that.data.dataForm3['rzsj' + k]
        delete that.data.dataForm3['lzsj' + k]
        delete that.data.dataForm3['zw' + k]
        delete that.data.dataForm3['xj' + k]
        delete that.data.dataForm3['lzyy' + k]
        delete that.data.dataForm3['dwzmr' + k]
        delete that.data.dataForm3['dwzmrdh' + k]
      }
      that.data.dataForm3['workExps'] = that.data.workExps;
    }

    console.log(that.data.dataForm3)
    that.data.dataForm['form3'] = that.data.dataForm3
    console.log(that.data.dataForm, 'that.data.dataForm');
    //提交
    that.openForm(that.data.dataForm)
  },
  // 上学起止时间
  starDateChange: function (e) {
    this.setData({
      star_date: e.detail.value
    })
  },
  endDateChange: function (e) {
    this.setData({
      end_date: e.detail.value
    })
  },
  // 下一页
  openForm: function (data) {
    wx.navigateTo({
      url: '/pages/resume/form4?data=' + JSON.stringify(data)
    })    
  },
  backPage: function () {
    wx.navigateBack({
    })
  },
  // 是否显示工作经历
  isShowWork: function (e) {
    console.log('e', e)
    if (e.currentTarget.dataset.name == 'isFalse') {
      console.log(e.currentTarget.dataset.name)
      this.setData({
        isShow: false
      })
    } else {
      this.setData({
        isShow: true
      })
    }
  },
  // 添加工作经历
  addWork: function () {
    var that = this;
    that.data.workExps.push({});
    this.setData({
      workExps: that.data.workExps
    })
  },
  // 工作经历改变
  inputChange: function (e) {
    console.log(e);
    var key = e.currentTarget.dataset.key;
    var index = e.currentTarget.dataset.index;
    var value = e.detail.value;
    var that = this;
    this.data.workExps[index][key] = value;
    this.setData({
      workExps: that.data.workExps
    })
  },
  // 移除工作经历
  remove: function (e) {
    var index = e.currentTarget.dataset.index;
    var that = this;
    that.data.workExps.splice(index, 1);
    that.setData({
      workExps: that.data.workExps
    })
  }
})
