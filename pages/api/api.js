export default {
  api:{
    MOBILE_ACCOUNT_REG__POST: "http://allqj.ngrok.qianjia.space/wechat/user_add", //  注册
    MOBILE_ACCOUNT_LOGIN_BYPASSWORD__POST: "http://allqj.ngrok.qianjia.space/wechat/login", // 密码登陆接口 手机验证码登陆
    MOBILE_ACCOUNT_LOGIN_BYSMSCODE__POST: "http://192.168.0.183/mobile/account/smslogin",
    MOBILE_ACCOUNT_SEND_SMSCODE__POST: "", //发送验证码123 http://allqj.ngrok.qianjia.space/wechat/send_code    
    MOBILE_ACCOUNT_PASSWORD_CHANGE__POST: "http://allqj.ngrok.qianjia.space/wechat/change_pwd",  // 修改密码
    MOBILE_ACCOUNT_PASSWORD_FORGOT__POST: "http://allqj.ngrok.qianjia.space/wechat/forgot_pwd", // 找回密码
    MOBILE_ACCOUNT_LOGOUT__POST: "http://allqj.ngrok.qianjia.space/wechat/logout", // 退出登陆
    MOBILE_ACCOUNT_SEND_MAIL__POST: "http://192.168.0.183/mobile/account/sendmail",
    MOBILE_ACCOUNT_STAUTS_GETCURRENT__POST: "http://192.168.0.183/mobile/account/status",
    MOBILE_ACCOUNT_UPLOAD_PHOTO__POST: "http://192.168.0.183/mobile/account/upload",
    MOBILE_ACCOUNT_MESSAGE_LIST__POST: "http://192.168.0.183/mobile/account/message",
    MOBILE_INDUCTION_LIST__POST: "http://192.168.0.183/mobile/account/induction",
    MOBILE_INDUCTION_SAVE__POST: "http://192.168.0.183/mobile/account/induction/save",
    MOBILE_RESUME_SAVE__POST: "http://192.168.0.183/mobile/resume/save",
    MOBILE_RESUME_TEMPSAVE__POST: "http://192.168.0.183/mobile/resume/tempsave",
    MOBILE_TEST_AQ_LIST__POST: "192.168.0.183/mobile/test/aq",
    MOBILE_TEST_EQ_LIST__POST: "192.168.0.183/mobile/test/eq",
    MOBILE_TEST_IQ_LIST__POST: "192.168.0.183/mobile/test/iq",
    MOBILE_TEST_HQ_LIST__POST: "http://192.168.0.183/mobile/test/hq",
    MOBILE_TEST_SAVE__POST: "http://192.168.0.183/mobile/test/save",
  }
}