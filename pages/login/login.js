import data from '../register/register.js'
import Api from '../api/api.js'
import base64 from '../../utils/base64.js'
import validate from '../../utils/vallidate.js'
const wxValidate = new validate()
const api = Api.api
const Base64 = new base64.Base64() // Base64.encode(a); Base64.decode(b);
Page({
  data: {
    // tab切换  
    currentTab: 0,
    checked: 'password',
    circular: true,
    duration: "300",
    focus: false,
    savepass:false,
    code:"",
    // 是否登录过
    loginFlag:false,
    trueCode:"",
    sendCode: "发送验证码",
    // 倒计时
    countFlag: false,
    required: ["phone",'password'],
    passIcon: false,
    passChecked: 'password',
    top:0

  },
  // 是否登录过
  onLoad: function () {
    wxValidate.addRule([{ codePhone: /^1[34578][0-9]{9}$/, msg: "手机号格式不正确" }])
    let mobile = wx.getStorageSync('mobile');
    let password = wx.getStorageSync('password');
    if (mobile && password) {
      this.data.form = {};
      this.data.form.$invalidMsg = true;
      this.setData({ loginFlag: true })
      this.setData({ mobile: mobile, password: Base64.decode(password), savepass: true })
    }
  },
  // 点击tab切换 
  swichNav: function (e) {
    let current = typeof e === 'number' ? e : e.currentTarget.dataset.current
    this.setData({ currentTab: current })
  },
  changeIndex: function (e) {
    this.swichNav(e.detail.current)
  },
  // 验证手机号
  validate: function (e) {
    wxValidate.validate(e,this)
    this.setData({ [e.currentTarget.dataset.name]: e.detail.value })
    this.setData({ top: '0' })
  },
  // 发送验证码
  send_code: function (e) {
    data.data.send_code(e,this).then(function(code){
      wxValidate.addRule([{ code: new RegExp(code), msg: "验证码有误！" }])
    })
  },
  // 手机号密码登录
  submitByPass: function (e) {
    wxValidate.checkRequired(e, this, this.data.required)
    let value = e.detail.value;
    let that = this;
    if (this.data.form.$invalidMsg != true) { return false; }
    else {
      wx.request({
        url: api['MOBILE_ACCOUNT_LOGIN_BYPASSWORD__POST'],
        method: "POST",
        data: {
          'mobile': value.phone,
          'password': Base64.encode(value.password),
          'type': '1'
        },
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        success: function (res) {
          if (that.data.savepass) {
            wx.setStorageSync('mobile', value.phone)
            wx.setStorageSync('password', Base64.encode(value.password))
          }
          if (res.data.code != 200) {
            data.data.showToast("用户名或密码错误")
          } else {
            wx.navigateTo({ url: `/pages/index/index?mobile=${value.phone}` })
          }
        },
        fail: function (res) {
          console.log(res)
        }
      })
    }
  },
  // 验证码登录
  submitByCode: function (e) {
    wxValidate.checkRequired(e, this, ['codePhone','code'])
    let value = e.detail.value;
    console.log(this)
    if (this.data.trueCode == '') { data.data.showToast('请发送验证码');return false;}
    else{
      if (this.data.form && this.data.form.codePhone == true && this.data.form && this.data.form.code == true) {
        wx.request({
          url: api['MOBILE_ACCOUNT_LOGIN_BYPASSWORD__POST'],
          method: "POST",
          data: {
            'mobile': value.codePhone,
            'type': '2',
            'code': value.code
          },
          header: {
            'content-type': 'application/x-www-form-urlencoded'
          },
          success: function (res) {
            if (res.data.code != 200) {
              data.data.showToast(res.data.msg)
            }
            else {
              wx.navigateTo({ url: `/pages/index/index?mobile=${value.codePhone}` })
            }
          },
          fail: function (res) {
            console.log(res)
          }
        })
      } 
    }
  },
  // 用开关控制密码是否加密
  checkPwd: function (e) {
    let falg = this.data[e.currentTarget.dataset.icon] === false ? true : false;
    this.setData({ [e.currentTarget.dataset.icon]: falg, [e.currentTarget.dataset.name]: falg === false ? 'password' : 'text' })
  },
  forgetpwd: function () {
    wx.navigateTo({ url: '/pages/forgetpwd/forgetpwd' })
  },
  register: function () {
    wx.navigateTo({ url: '/pages/register/register' }) 
  },
  // 保存密码
  savepwd: function (e) {
    e.detail.value[0] === "savepwd" ? this.setData({savepass: true}) :
    this.setData({ savepass: false,loginFlag: false })
    wx.removeStorageSync('mobile')
    wx.removeStorageSync('password');
  },
  scrollTop: function () {
    this.setData({top:'-50rpx'})
  }
})